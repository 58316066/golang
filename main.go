package main

import (
    "fmt"
    "strconv" // import เมื่อแปลง int => string
    "database/sql"
    _"github.com/go-sql-driver/mysql"
)

// ประกาศเมื่อต้องการจะ Select
type tb struct{
    Id int `json:"id_u"`
    Fname string `json:"Fname"`
    Lname string `json:"lname"`
    Nickname string `json:"nickname"`
}

func main() {
    fmt.Println("Go MySQL Tutorial")

    // Open up our database connection.
    // I've set up a database on my local machine using phpmyadmin.
    // The database is called testDb
	db, err := sql.Open("mysql", "root:0862094557@tcp(127.0.0.1:3307)/dbtest")

    // if there is an error opening the connection, handle it
    if err != nil {
        panic(err.Error())
    }

    // defer the close till after the main function has finished
    // executing
    defer db.Close()
    
	//insert DB
	insert, err := db.Query("INSERT INTO tb_name (id_u, fname, lname, nickname) VALUES(0,'nakhon','sawan','park2')")

	if err != nil{
		panic(err.Error())
	}

    defer insert.Close()
    

    //select DB
    results, err := db.Query("SELECT * FROM tb_name")

    if err != nil{
        panic(err.Error)
    }

    for results.Next(){
        var name tb

        err = results.Scan(&name.Id,&name.Fname,&name.Lname,&name.Nickname)
        if err != nil{
            panic(err.Error())
        }

        s := strconv.Itoa(name.Id) //แปลง int => string //strconv.Itoa(199)

    fmt.Println(s+" "+name.Fname+" "+name.Lname+" Nickname is "+name.Nickname)
    }



	fmt.Println("Success Full")

}